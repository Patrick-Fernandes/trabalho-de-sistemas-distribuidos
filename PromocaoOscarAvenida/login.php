<?php session_start(); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="generator" content="">
        <title>Login</title>
        <!-- Bootstrap core CSS -->
        <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://getbootstrap.com/docs/4.3/examples/sign-in/signin.css" rel="stylesheet">

        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <meta name="google-signin-client_id" content="616809964020-7ad7nvioj1amnvseurae2qq558e7b1b7.apps.googleusercontent.com">

        <!-- Custom styles for this template -->
    </head>
    <body class="text-center">



        <form action="Controler/validacao.php" method="post" class="form-signin">
            <fieldset>
                <h1 class="h3 mb-12 font-weight-normal">Faça seu login 
                </h1>
                <h5>  <?php 
                    if(isset($_SESSION['loginErro'])){
                        ?> <?=  $_SESSION['loginErro'] ?><?php
                    }
                    ?></h5>
                <label for="txUsuario" for="inputEmail" class="sr-only">E-mail</label>
                <input type="text" name="email" id="txUsuario" maxlength="60" 
                       type="email" id="inputEmail" class="form-control" 
                       placeholder="Email address" required autofocus/>
                <label for="txSenha" for="inputPassword" class="sr-only">Senha</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required
                       name="senha" id="txSenha" />
                <input type="submit" value="Entrar" class="btn btn-lg btn-primary btn-block" />
                <br>
                
                <div class="g-signin2 btn" data-onsuccess="onSignIn"></div>   <br> <br>        
                <p><a href="Cadastro" class="btn btn-primary">Cadastra-se</a></p>

                
                <script>
                  
                    function onSignIn(googleUser) {
                        var profile = googleUser.getBasicProfile();
                        var userID = profile.getId(); // Do not send to your backend! Use an ID token instead.
                        var userName = profile.getName();
                        var userImg = profile.getImageUrl();
                        var userEmail = profile.getEmail(); // This is null if the 'email' scope is not present.
                        var userToken = googleUser.getBasicProfile().id_token;

                        //   document.getElementById('msg').innerHTML = userEmail;

                        if (userEmail !== "") {
                            var dados = {
                                userID: userID, userName: userName, userImg: userImg, userEmail: userEmail
                            };

                            $.post('Controler/validaGoogle.php', dados, function (retorna) {
                                if (retorna === 'home') {
                                    alert('Bem Vindo');

                                    window.location.href = retorna;
                                } else {
                                    document.getElementById('msg').innerHTML = retorna;

                                }
                                //                         

                            });
                        } else {
                            var msg = "Usuario não encontrado";
                            document.getElementById('msg').innerHTML = msg;
                        }
                    }

                </script>
                <br>
                <p class="mt-5 mb-3 text-muted" id="msg">&copy; 2017-2019</p>
            </fieldset>

        </form>
        <!--google informações-->




        <!--google fim-->


    </body>
</html>
<?php
$_SESSION['loginErro'] ='';

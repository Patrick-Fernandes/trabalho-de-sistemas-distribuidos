<!--TESTA  CPF VALIDO de imput-->
    <script type="text/javascript" >
function TestaCPF(strCPF) {
                            var Soma;
                            var Resto;
                            Soma = 0;
                            var novoCPF = strCPF.replace(/[\.-]/g, "");
                            
                            if (novoCPF == "00000000000")
                                return alert('CPF invalido');

                            for (i = 1; i <= 9; i++)
                                Soma = Soma + parseInt(novoCPF.substring(i - 1, i)) * (11 - i);
                            Resto = (Soma * 10) % 11;

                            if ((Resto == 10) || (Resto == 11))
                                Resto = 0;
                            if (Resto != parseInt(novoCPF.substring(9, 10)))
                                return alert('CPF invalido');

                            Soma = 0;
                            for (i = 1; i <= 10; i++)
                                Soma = Soma + parseInt(novoCPF.substring(i - 1, i)) * (12 - i);
                            Resto = (Soma * 10) % 11;

                            if ((Resto == 10) || (Resto == 11))
                                Resto = 0;
                            if (Resto != parseInt(novoCPF.substring(10, 11)))
                                return alert('CPF invalido');
                            return true;
                        }
            </script>                    
                            
    <!-- Adicionando Javascript do busca cep -->
    <script type="text/javascript" >
    
    function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('uf').value=("");
          //  document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
          //  document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
        
    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('uf').value="...";
               // document.getElementById('ibge').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };

    </script>
        <!-- fim Javascript -->

<?php

function classificacao($i) {
    if ($i == 'l' || $i == 'L') {
        return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/DJCTQ_-_L.svg/75px-DJCTQ_-_L.svg.png";
    } else if ($i == '10') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/DJCTQ_-_10.svg/75px-DJCTQ_-_10.svg.png';
    } else if ($i == '12') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/DJCTQ_-_12.svg/75px-DJCTQ_-_12.svg.png';
    } else if ($i == '14') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/DJCTQ_-_14.svg/75px-DJCTQ_-_14.svg.png';
    } else if ($i == '16') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/DJCTQ_-_16.svg/75px-DJCTQ_-_16.svg.png';
    } else if ($i == '18') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/DJCTQ_-_18.svg/75px-DJCTQ_-_18.svg.png';
    } else {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/DJCTQ_-_ER.svg/75px-DJCTQ_-_ER.svg.png';
    }
}

function verificaCadadastro($id) {

    include '../Controler/conexao.php';
    $sqlBC = "SELECT `cpf`, `dataNascimento`, `telefone`, `telefone2`,
            `cep`, `endereco`, `numero`, `bairro`, `estado`, `cidade`, 
            `NomeCompleto` 
            fROM 
            `dadoscadastrais` WHERE `id_usuario` =$id";
    $resultBC = mysqli_query($conn, $sqlBC);
    if (mysqli_num_rows($resultBC) > 0) {
        $rowBC2 = mysqli_fetch_assoc($resultBC);
        if ($rowBC2["cpf"] == '000.000.000-00' || $rowBC2["dataNascimento"] == '00/00/000' ||
                $rowBC2["telefone"] == '(00) 00000-0000'  ||
                $rowBC2["cep"] == '' ||$rowBC2["endereco"] == '' ||$rowBC2["bairro"] == '' ||$rowBC2["estado"] == '' 
                ||$rowBC2["cidade"] == '' ||$rowBC2["NomeCompleto"] == '' ) {
            return "erro";
        } else {
            return "valido";    
        }
    }else {
        return "erro";
    }
}

function contaVotosFilmes($id, $idVoto) {
    if ($idVoto != 'null') {
        include '../Controler/conexao.php';
        $sqlBC = "SELECT `NomeFilme`,`LinkImgFilme`,`GeneroFilme`,idFilme, 
                                    COUNT(idFilme) AS Qtd FROM voto  where `idFilme` = $id and tipo_id =$idVoto 
                                    GROUP BY idFilme ORDER BY COUNT(`idFilme`) DESC ";
        $resultBC = mysqli_query($conn, $sqlBC);
        if (mysqli_num_rows($resultBC) > 0) {
            while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
                return "Nº de votos nesta catégoria " . $rowBC2["Qtd"];
            }
        } else {
            return "Nº de votos nesta catégoria 0";
        }
    } else {
        echo 'Escolha uma catégoria';
    }
}

function trazFilme($id) {

    include '../Controler/conexao.php';
    $sqlBC = " SELECT voto.id as idvoto,voto.`mensagem`,
                           voto.`NomeFilme`,voto.`LinkImgFilme`,voto.`GeneroFilme`,voto.`idFilme`
                           FROM voto
                           where `idFilme` =$id";
    $resultBC = mysqli_query($conn, $sqlBC);
    if (mysqli_num_rows($resultBC) > 0) {
        while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
            return $rowBC2['NomeFilme'];
        }
    } else {
        return false;
    }
}

function trazTipo($id) {

    include '../Controler/conexao.php';
    $sqlBC = "   SELECT `id` , nome
                  FROM `tipo` WHERE `id` = $id";
    $resultBC = mysqli_query($conn, $sqlBC);
    if (mysqli_num_rows($resultBC) > 0) {
        while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
            return $rowBC2['nome'];
        }
    } else {
        return false;
    }
}

function verificaSeJaVotou($idUser, $idTipo) {

    include '../Controler/conexao.php';
    $sqlBC = "   SELECT `idFilme` 
                  FROM `voto` WHERE `user_id` = $idUser
                           and `tipo_id` = $idTipo";
    $resultBC = mysqli_query($conn, $sqlBC);
    if (mysqli_num_rows($resultBC) > 0) {
        while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
            return true;
        }
    } else {
        return false;
    }
}

function tirarAcento($stringExemplo) {
    $comAcentos = array(' ', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë',
        'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á',
        'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó',
        'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');

    $semAcentos = array('-', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e',
        'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E',
        'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O',
        'O', '0', 'U', 'U', 'U');


    return str_replace($comAcentos, $semAcentos, $stringExemplo);
}

function colocaAcento($stringExemplo) {
    $semAcentos = array('-', 'direcao');


    $comAcentos = array(' ', 'direção');
    return str_replace($semAcentos, $comAcentos, $stringExemplo);
}

function limpaURL($stringExemplo) {
    $semAcentos = array('/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida/Melhor-', 'PromocaoOscarAvenida/Melhor-');


    $comAcentos = array('', '');
    return str_replace($semAcentos, $comAcentos, $stringExemplo);
}

function topoVotos($id) {
    include '../Controler/conexao.php';
    $sqlFILME = "SELECT `NomeFilme`,`LinkImgFilme`,`GeneroFilme`,idFilme, COUNT(idFilme)
        AS Qtd
        , tipo.nome as tipoVoto
        FROM voto 
        INNER JOIN tipo on voto.tipo_id = tipo.id
        where tipo_id =$id GROUP BY idFilme ORDER BY COUNT(`idFilme`) DESC limit 1";
    $resultFILME = mysqli_query($conn, $sqlFILME);
    return $resultFILME;
}

function filmesQVotei($id) {
    include '../Controler/conexao.php';
    $sqlFILME = "SELECT voto.id,mensagem, `NomeFilme`,`LinkImgFilme`,`GeneroFilme`,idFilme, tipo.nome as tipoVoto
        FROM voto 
        INNER JOIN tipo on voto.tipo_id = tipo.id
        where `user_id` =$id";
    $resultFILME = mysqli_query($conn, $sqlFILME);
    return $resultFILME;
}
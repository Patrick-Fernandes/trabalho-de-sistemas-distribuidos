<?php
session_start();
if ($_SESSION['usuarioNiveisAcessoId'] != 3) {
    header('Location: home');
} else {
    include '../Controler/conexao.php';
    include '../Controler/Funcoes.php';
    ?>


    <!DOCTYPE html>
    <html lang="en">

        <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>Área administrativa</title>

            <!-- Custom fonts for this template-->
            <link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <script src="admin/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Custom styles for this page -->
  <link href="admin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!-- Page level custom scripts -->
  <script src="admin/js/demo/datatables-demo.js"></script>
            <!-- Custom styles for this template-->
            <link href="admin/css/sb-admin-2.min.css" rel="stylesheet">

        </head>

        <body id="page-top">

            <!-- Page Wrapper -->
            <div id="wrapper">

                <!-- Sidebar -->
                <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                    <!-- Sidebar - Brand -->
                    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                        <div class="sidebar-brand-icon rotate-n-15">
                            <i class="fas fa-laugh-wink"></i>
                        </div>
                        <div class="sidebar-brand-text mx-3"><?= $_SESSION['usuarioNome'] ?></div>
                    </a>

                    <!-- Divider -->
                    <hr class="sidebar-divider my-0">
                    <!-- Nav Item - Dashboard -->


                    <li class="nav-item active">
                        <a class="nav-link" href="ADM">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            <span>Home</span></a>
                    </li>               
                    <li class="nav-item active">
                        <a class="nav-link" href="Home">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            <span>Voltar para concurso</span></a>
                    </li>
                    
                    <!-- Divider -->
                    <hr class="sidebar-divider">
                    <!-- Nav Item - Pages Collapse Menu -->
                    
                    
                    
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i class="fas fa-fw fa-folder"></i>
                            <span>Votos</span>
                        </a>
                        
                    </li>
                        <?php
                $sqlBC = "SELECT * FROM `tipo`";
                $resultBC = mysqli_query($conn, $sqlBC);
                $pen = 0;
                $idVoto = 'null';
                if (mysqli_num_rows($resultBC) > 0) {
                    // output data of each row
                    while ($rowBC = mysqli_fetch_assoc($resultBC)) {
                      ?> 
                        
                           <li class="nav-item ">
                        <a class="nav-link" href="Mais-Votado-<?=$rowBC["id"] ?> ">
                            
                            <span>Votos do Melhor <?=$rowBC["nome"] ?> </span></a>
                    </li>
                 
                        
                          
                          <?php
                    }
                } else {
                    echo "0 results";
                }
                ?>
                     
                     
                        
<!--                        
                        
                     Nav Item - Tables 
                    <li class="nav-item">
                        <a class="nav-link" href="Perfil-<?= $_SESSION['usuarioId'] ?>">
                            <i class="fas fa-fw fa-table"></i>
                            <span>Meu Perfil</span></a>
                    </li>
                    -->
                    

                    <!-- Divider -->
                    <hr class="sidebar-divider d-none d-md-block">

                    <!-- Sidebar Toggler (Sidebar) -->
                    <div class="text-center d-none d-md-inline">
                        <button class="rounded-circle border-0" id="sidebarToggle"></button>
                    </div>

                </ul>
                <!-- End of Sidebar -->

                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">

                    <!-- Main Content -->
                    <div id="content">

                        <!-- Topbar -->
                        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                            <!-- Sidebar Toggle (Topbar) -->


                            <!-- Topbar Search -->


                        </nav>
                        <!-- End of Topbar -->

                    <?php } ?>



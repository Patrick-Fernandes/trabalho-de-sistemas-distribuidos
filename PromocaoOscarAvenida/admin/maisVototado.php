<?php
include_once './topo.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = 0;
}
?>



<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Lista dos votos o Melhor <?= trazTipo($id); ?> </h1>

    <p class="mb-4">
        lista todos os voto da categoria  Melhor <?= trazTipo($id); ?><br>
        

    </p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Melhor <?= trazTipo($id); ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID filme</th>
                            <th>Nome do filme </th>
                            <th>Capa</th>
                            <th>Gênero</th>
                            <th>Nº votos</th>
                            <th>Catégoria </th>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        $sqlBC = "SELECT `NomeFilme`,`LinkImgFilme`,`GeneroFilme`,idFilme, COUNT(idFilme) AS Qtd , 
        tipo.nome as tipoVoto FROM voto INNER JOIN tipo on voto.tipo_id = tipo.id where tipo_id =$id
        GROUP BY idFilme ORDER BY COUNT(`idFilme`) DESC ";
                        $resultBC = mysqli_query($conn, $sqlBC);
                        if (mysqli_num_rows($resultBC) > 0) {
                            while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
                                ?>  <tr>
                                    <td><a href="Categoria-<?=$id?>-Filme-<?=   $rowBC2["idFilme"] ?>"><?=   $rowBC2["idFilme"] ?></a> </td>
                                    <td><?=   $rowBC2["NomeFilme"] ?> </td>
                                    <td><img src="<?=   $rowBC2["LinkImgFilme"] ?>" width="100" height="150"></td>
                                    <td><?=   $rowBC2["GeneroFilme"] ?></td>
                                    <td><?=   $rowBC2["Qtd"] ?></td>
                                    <td><?=   $rowBC2["tipoVoto"] ?></td>
                                </tr>
                                <?php
                              ;
                            }
                        } else {
                            ?> <tr>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado</td>
                                <td>nenhum registro encontrado</td>
                                <td>nenhum registro encontrado</td>
                            </tr> <?php
                        }
                        ?>



                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php
include './rodape.php';

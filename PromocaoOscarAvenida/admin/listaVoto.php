<?php
include_once './topo.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = 0;
}

 $sqlBC = " SELECT u.`id`, u.`nome`,  u.`email`,  u.`id_google` ,

 
 dc.`cpf`, dc.`dataNascimento`, dc.`telefone`, dc.`telefone2`,
 dc.`cep`, dc.`endereco`, dc.`numero`, dc.`complemento`,
 dc.`bairro`, dc.`estado`, dc.`cidade`,dc.`NomeCompleto`,
 
v.`mensagem`, v.`NomeFilme`, v.`LinkImgFilme`,
v.`GeneroFilme`, v.`idFilme`, t.nome as categoria

FROM `usuario` u

left join  dadoscadastrais dc on u.id = dc.id_usuario
left join  voto v on u.id = v.user_id
left join  tipo t on v.tipo_id = t.id

WHERE v.id = $id";
$resultBC = mysqli_query($conn, $sqlBC);
if (mysqli_num_rows($resultBC) > 0) {
    $rowBC2 = mysqli_fetch_assoc($resultBC);
     $rowBC2['NomeFilme'];
    ?>
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-0 text-gray-800">
            Nome de Login:   <?= $rowBC2['nome']; ?>
        </h1>


        <p class="mb-4">Mensagem: <?= $rowBC2['mensagem']; ?></p>
        <div class="col-lg-12 mb-4">
            <!-- Approach -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Dados pessoais:</h6>
                </div>
                <div class="card-body">
                    <div class="col-lg-6 mb-2" style="float: left">

                        <p class="mb-0"><b>Nome Completo: </b><?= $rowBC2['NomeCompleto']; ?> </p>
                        <p class="mb-0"><b>CPF:  </b><?= $rowBC2['cpf']; ?> </p>
                        <p class="mb-0"><b> Telefone: </b><?= $rowBC2['telefone']; ?> </p>
                        <p class="mb-0"><b> Telefone: </b><?= $rowBC2['telefone2']; ?> </p>
                        <p class="mb-0"><b> Data de Nascimento </b><?= $rowBC2['dataNascimento']; ?> </p>
                    </div>


                    <div class="col-lg-6 mb-2" style="float: left">
                        <p class="mb-0"><b>E-mail:  </b><?= $rowBC2['email']; ?> </p>
                        <p class="mb-0"><b>CEP:  </b><?= $rowBC2['cep']; ?> </p>
                        <p class="mb-0"><b>Endereço:  </b><?= $rowBC2['endereco']; ?> </p>
                        <p class="mb-0"><b>Nº  </b><?= $rowBC2['numero']; ?> </p>
                        <p class="mb-0"><b>Complemento:  </b><?= $rowBC2['complemento']; ?> </p>
                    </div>
                    <div class="col-lg-6 mb-2" style="float: left">
                        <p class="mb-0"><b>Bairro: </b><?= $rowBC2['bairro']; ?> </p>
                        <p class="mb-0"><b>Estado:  </b><?= $rowBC2['estado']; ?> </p>
                        <p class="mb-0"><b>Cidade:  </b><?= $rowBC2['cidade']; ?> </p>
                    </div>
                    <p class="mb-0"><b>Usuario Google:  </b><?php
                        if ($rowBC2['id_google'] != null) {
                            echo 'sim';
                        } else {
                            echo 'não';
                        };
                        ?> </p>
                    <p class="mb-0"><b>Id do Google:  </b><?= $rowBC2['id_google']; ?> </p>
                </div>
            </div>
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Dados do filme e catégoria:</h6>
                </div>
                <div class="card-body">
                    <div class="text-center">      
                        <h4><?= $rowBC2['NomeFilme']; ?></h4>
                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4"
                             style="width: 250px; height: 350px" src="<?= $rowBC2['LinkImgFilme']; ?>" 
                             alt="<?= $rowBC2['NomeFilme']; ?>">
                    </div>
                                        <p class="mb-0"><b>Catégoria: </b> Melhor <?= $rowBC2['categoria']; ?> </p>

                    
                    <p class="mb-0"><b>Gênero: </b><?= $rowBC2['GeneroFilme']; ?> </p>


                    <p class="mb-0"><b>Mensagem: </b><?= $rowBC2['mensagem']; ?> </p>


                </div>
            </div>
        </div>



    </div>

    <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <?php
} else {
    
}
include './rodape.php';

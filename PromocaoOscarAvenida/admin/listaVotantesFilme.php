<?php
include_once './topo.php';
if (isset($_GET['idCategoria']) && isset($_GET['idfilme'])) {
    $idFilme = $_GET['idfilme'];
    $idCategoria = $_GET['idCategoria'];
} else {
    $id = 0;
}
?>



<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-0 text-gray-800">Lista dos votos do <?= trazFilme($idFilme); ?> </h1>

    <p class="mb-4">
        Lista dos que votaram no <?= trazFilme($idFilme); ?> 
        como melhor <?= trazTipo($idCategoria); ?>
    </p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> lista dos que votaram no <?= trazFilme($idFilme); ?> 
                como melhor <?= trazTipo($idCategoria); ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID voto</th>
                            <th>Nome do filme </th>
                            <th>Capa</th>

                            <th>Id Usuário </th>
                            <th>Usuário</th>
                            <th>E-mail</th>
                            <th>CPF</th>
                            <th>Data de Nasc.</th>

                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        $sqlBC = "SELECT voto.id as idvoto,voto.`mensagem`,
                           voto.`NomeFilme`,voto.`LinkImgFilme`,voto.`idFilme`,
                           usuario.nome, usuario.email, usuario.id as identUser,
                           tipo.nome as tipoVoto ,
                           dc.cpf, dc.dataNascimento,dc.telefone
                           FROM voto
                           INNER JOIN tipo on voto.tipo_id = tipo.id
                           INNER JOIN usuario on voto.`user_id` = usuario.id
                           left join  dadoscadastrais dc on usuario.id = dc.id_usuario

                           where tipo_id =$idCategoria and `idFilme` =$idFilme 
                               order by   dc.cpf desc
                              ";
                        $resultBC = mysqli_query($conn, $sqlBC);
                        if (mysqli_num_rows($resultBC) > 0) {
                            while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
                                ?>
                                <tr>

                                    <td><a href="Voto-<?= $rowBC2["idvoto"] ?>"><?= $rowBC2["idvoto"] ?></a> </td>
                                    <td <?php
                                    if ($rowBC2["cpf"] == '000.000.000-00') {
                                        echo'style="   color: red"';
                                    }
                                    ?>><?= $rowBC2["NomeFilme"] ?> 
                                    </td>
                                    <td><img src="<?= $rowBC2["LinkImgFilme"] ?>" width="100" height="150"></td>
                                    <td <?php
                            if ($rowBC2["cpf"] == '000.000.000-00') {
                                echo'style="   color: red"';
                            }
                                    ?>><?= $rowBC2["identUser"] ?>
                                    </td>
                                    <td <?php
                                    if ($rowBC2["cpf"] == '000.000.000-00') {
                                        echo'style="   color: red"';
                                    }
                                    ?>><?= $rowBC2["nome"] ?>
                                    </td>
                                    <td  <?php
                                        if ($rowBC2["cpf"] == '000.000.000-00') {
                                            echo'style="   color: red"';
                                        }
                                        ?>><?= $rowBC2["email"] ?>
                                    </td>

                                    <td  <?php
                            if ($rowBC2["cpf"] == '000.000.000-00') {
                                echo'style="   color: red"';
                            }
                                        ?>><?= $rowBC2["cpf"] ?>
                                    </td>
                                    <td  <?php
                        if ($rowBC2["dataNascimento"] == '00/00/0000') {
                            echo'style="   color: red"';
                        }
                        ?>><?= $rowBC2["dataNascimento"] ?>
                                    </td>


                                </tr>
                                <?php
                            }
                        } else {
                            ?> <tr>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado </td>
                                <td>nenhum registro encontrado</td>
                                <td>nenhum registro encontrado</td>
                                <td>nenhum registro encontrado</td>
                            </tr> <?php
                        }
                        ?>



                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php
include './rodape.php';

<?php include './Cabecalho.php'; 

if (!isset($_SESSION['usuarioId'])) {
    echo '<script>location.href = "login";</script>';
}
 $sqlBC = " SELECT u.`id`, u.`nome`,  u.`email`,  u.`id_google` ,
 dc.`cpf`, dc.`dataNascimento`, dc.`telefone`, dc.`telefone2`,
 dc.`cep`, dc.`endereco`, dc.`numero`, dc.`complemento`,
 dc.`bairro`, dc.`estado`, dc.`cidade`,dc.`NomeCompleto`
 
FROM `usuario` u

left join  dadoscadastrais dc on u.id = dc.id_usuario

WHERE u.id = ".$_SESSION['usuarioId'];
$resultBC = mysqli_query($conn, $sqlBC);
if (mysqli_num_rows($resultBC) > 0) {
    $rowBC2 = mysqli_fetch_assoc($resultBC);
    ?>

<style>
    p{
        font-size: 20px;
    }
</style>
<main role="main" class="container">
    <div class="starter-template">
        <h1>Perfil de <?= $_SESSION['usuarioNome'] ?> </h1>
    
        <div class="col-lg-12 mb-4">
            <!-- Approach -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary"> Dados pessoais:</h4>
                </div>
                <div class="card-body">

                        <p class="mb-0"><b>Nome Completo: </b><?= $rowBC2['NomeCompleto']; ?> </p>
                        <p class="mb-0"><b>CPF:  </b><?= $rowBC2['cpf']; ?> </p>
                        <p class="mb-0"><b> Telefone: </b><?= $rowBC2['telefone']; ?> </p>
                        <p class="mb-0"><b> Telefone: </b><?= $rowBC2['telefone2']; ?> </p>
                        <p class="mb-0"><b> Data de Nascimento </b><?= $rowBC2['dataNascimento']; ?> </p>
     
                        <p class="mb-0"><b>E-mail:  </b><?= $rowBC2['email']; ?> </p>
                        <p class="mb-0"><b>CEP:  </b><?= $rowBC2['cep']; ?> </p>
                        <p class="mb-0"><b>Endereço:  </b><?= $rowBC2['endereco']; ?> </p>
                        <p class="mb-0"><b>Nº  </b><?= $rowBC2['numero']; ?> </p>
                        <p class="mb-0"><b>Complemento:  </b><?= $rowBC2['complemento']; ?> </p>
     
                        <p class="mb-0"><b>Bairro: </b><?= $rowBC2['bairro']; ?> </p>
                        <p class="mb-0"><b>Estado:  </b><?= $rowBC2['estado']; ?> </p>
                        <p class="mb-0"><b>Cidade:  </b><?= $rowBC2['cidade']; ?> </p>
                 
                    <p class="mb-0"><b>Usuario Google:  </b><?php
                        if ($rowBC2['id_google'] != null) {
                            echo 'sim';
                        } else {
                            echo 'não';
                        };
                        ?>
                    </p>
                    <p><a class="btn btn-block btn-facebook" href="Editar-Perfil">Editar Perfil</a> </p>
                </div>
            </div>
        </div>
        <div class="row">
        </div>

     
    </div>
</main><!-- /.container -->



<?php
}
include 'Rodape.php';

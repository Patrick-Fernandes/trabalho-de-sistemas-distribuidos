<?php include './Cabecalho.php'; ?>



<main role="main" class="container">

    <div class="starter-template">
        <h1>Regulamento </h1>
        <p class="lead">

        <p><b>“PROMOÇÃO OSCAR AVÊNIDA”</b></p>
        <p><b>CERTIFICADO DE AUTORIZAÇÃO CAIXA Nº 2-8466/2019</b></p>
        </span><br><br>
        <p><b>1.</b> <b>Empresa Promotora. MONDELEZ BRASIL LTDA.,</b> 
            doravante denominada simplesmente “PROMOTORA”, inscrita no CNPJ/
            MF nº. 33.033.028/0001-84, com sede na Avenida Presidente Kennedy, 
            2511 parte, Água Verde, CEP 80.610-010, Município de Curitiba, Estado do Paraná, Brasil.</p><br>

        <p><b>2.</b> <b>Modalidade da Distribuição Gratuita de Prêmios.</b> A presente 
            distribuição gratuita de prêmios realizar-se-á na modalidade vale-brinde.</p><br>


        <p><b>3. Distribuição de prêmios.</b>A presente promoção será válida, única e exclusivamente,
            para consumidores pessoas físicas - com idade igual ou superior aos dezesseis (16) 
            anos completos na data do cadastro, residentes e domiciliadas no território nacional.</p><br>

        <p><b>4. Licença de uso.</b> Neste site tem a liberação do Grupo Cine Avenida Ltda. para usar e consumir os dados disponibilizados na 
            <a href="http://localhost/trabalho-de-sistemas-distribuidos/Cinema-Avenida/api">API</a>
        oferecida pela grupo, sendo todos os dados referentes aos filmes de toda e total responsabilidade de seus idealizados.</p><br>


        <p><b>5.</b> <b>Prazo de Execução.</b></p><br>
        <p><b>5.1.</b> A promoção iniciar-se-á no dia 01/01/2019 e encerrar-se-á no dia 30/06/2019. </p><br>
        <p><b>6. Descrição da Mecânica.</b></p><br>
        <p><b>6.1.1.</b> A PROMOTORA declara que a quantidade de vale-brindes produzidos corresponde a
            quantidade de prêmios a serem distribuídos.</p><br>
        <p><b>6.2. Condição e forma de participação.</b> Durante o período de participação válido
            (item 5.1), o consumidor pessoa física - com idade igual ou superior aos dezesseis (16)
            anos de idade, residente e domiciliada no território nacional interessado em participar.</p><br>
        <p><b>6.2.1.</b> Os consumidores interessados com idade entre dezesseis (16) e dezoito (18) anos de idade
            completos na data de cadastro estarão autorizados a participar e a eventualmente receber prêmio 
            desta promoção, desde que assistidos por seus pais ou responsáveis legais e atendidos os
            demais requisitos desse Regulamento.</p><br>
        <p><b>6.2.2.</b> Para saber se está concorrendo o participante deverá completar o cadstro e enviar uma mensagem 
            contando um história e/ou motivo para que aquele filme seja escolido.</p><br>

        <p><b>6.2.3. Condição para entrega do prêmio.</b> <u>Como condição para entrega do prêmio, a 
                PROMOTORA exigirá do contemplado o cadastro completo de seus dados CPF, Data de nascimento, endereço  bem 
                como o cumprimento das etapas do item 6.2.4, sob pena de perda do direito ao prêmio</u>.</p><br>
        <p><b>6.2.4.</b> Para resgatar o prêmio descrito no vale-brinde, 
            o consumidor deverá acessar o site <a href="http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida/">
                <u>http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida/</u></a>, realizar o seu cadastro e cumprir 
            as etapas de validação de sua premiação, nos termos abaixo:</p><br><br>
        <p><b>Campos de cadastro:</b></p><br>
        <p>&bull; CPF</p>
        <p>&bull; Nome completo</p>
        <p>&bull; Data de nascimento (dd/mm/aaaa)</p>
        <p>&bull; Endereço completo com CEP</p>
        <p>&bull; DDD/número de celular</p>
        <p>&bull; E-mail (confirmação de e-mail)</p>
        <p>&bull; Senha (confirmação de senha)</p>
        <p>&bull; Assinalar a opção “Li e concordo com os termos do Regulamento desta promoção e; 
            ao participar da promoção, aceito receber mensagem da promoção<b>.</b></p><br><br>

        <p><b>7. Prescrição do direito aos prêmios.</b> Os prêmios disponibilizados para a promoção que não forem distribuídos, ou reclamados por quem de direito, em até cento e oitenta (180) dias após o término da promoção, serão declarados como prescritos, e os seus valores serão recolhidos, pela promotora, ao Tesouro Nacional, na forma de renda da União, no prazo de 10 (dez) dias.</p><br>
        <p><b>7.1.</b> A PROMOTORA não poderá ser responsabilizada por eventuais danos ocasionados pela aceitação ou utilização do prêmio.</p><br>
        <p><b>8. Canais e formas de divulgação institucional pela mídia.</b> A divulgação da promoção, para o conhecimento de suas condições, pelos participantes, realizar-se-á por meio de material impresso nos pontos de venda e no hotsite www.promocaotang.com.br, além de outras mídias itinerantes (redes sociais, etc.).</p><br>
        <p><b>8.1.</b> O regulamento completo estará disponível no hotsite http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida</p><br>
        <p><b>8.2.</b> O número do Certificado de Autorização constará de forma clara e precisa no site http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida Para os demais materiais de divulgação, a empresa Promotora solicita dispensa da aposição, fazendo constar, apenas, a indicação de consulta do número de Autorização CAIXA no referido site.</p><br>
        <p><b>9.</b> <b>Esclarecimentos sobre a promoção.</b> As dúvidas dos participantes sobre a presente promoção poderão ser esclarecidas através do SAC telefone 0800 704 1940, horário de atendimento: de segunda à sexta-feira, das 09h00 às 18h00 (exceto feriados).</p><br>
        <p><b>10. Comissão de julgamento.</b> As dúvidas e controvérsias oriundas dos consumidores participantes da promoção comercial serão, primeiramente, dirimidas por seus respectivos organizadores, persistindo-as, estas serão submetidas à REPCO/CAIXA, e no silêncio injustificado, os participantes poderão a qualquer momento, durante o período da promoção, fazer reclamação, desde que fundamentadas, aos órgãos de defesa dos direitos do consumidor de sua cidade ou Estado.</p><br>
        <p><b>11. Cláusulas de Exclusão de Responsabilidade.</b> O provimento de condições apropriadas de acesso à rede de Internet é de responsabilidade da prestadora de serviços contratada pelo participante para tal finalidade (provedor), sendo de responsabilidade da PROMOTORA apenas a manutenção e disponibilização do hotsite http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida</p><br>
        <p><b>11.1.</b> A PROMOTORA não será responsável por problemas técnicos que impeçam, retardem ou prejudiquem o envio ou recebimento das informações enviadas para o mencionado endereço eletrônico.</p><br>
        <p><b>11.2.</b> Em caso de perda de conexão à rede de Internet, no momento da realização do cadastro ou no envio das informações, não será devida qualquer indenização por parte da PROMOTORA, tendo o participante que aceitar a implicação da eventual falha.</p><br>
        <p><b>11.3. Hipótese excepcional - Suspensão da Promoção.</b> Havendo interrupção da promoção ou na publicação do hot site promocional por problemas de acesso à rede de <i>Internet</i>, intervenção de <i>hackers</i>, vírus, manutenção, queda de energia, falha de <i>software</i> ou <i>hardware</i>, bem como por caso fortuito ou força maior, não será devida qualquer indenização, devendo a PROMOTORA, entretanto, dar prosseguimento aos serviços tão logo haja a regularização do sistema, nos moldes originalmente propostos, ou seja, sem prorrogação de datas.</p><br>
        <p><b>11.4.</b> Também fica excluída a responsabilidade da PROMOTORA por qualquer dano relacionado a esta Promoção em função de eventuais falhas causadas pelos serviços dos Correios.</p><br>
        <p><b>12. Cessão provisória dos direitos de personalidade do ganhador em relação à promoção.</b> Os ganhadores desta promoção autorizarão, sem quaisquer ônus, a PROMOTORA a utilizar suas imagens, nomes e/ou vozes, em fotos, cartazes, filmes, bem como em qualquer tipo de mídia e peças promocionais para a divulgação da conquista do prêmio, no prazo de até um (1) ano após o término da promoção.</p><br>
        <p><b>12.1.</b> As autorizações descritas acima não implicam em qualquer obrigação de divulgação ou de pagamento de qualquer quantia por parte da PROMOTORA aos ganhadores.</p><br>
        <p><b>12.2.</b> À empresa regularmente autorizada nos termos da Lei 5.768/71, é deferida a formação de cadastro e/ou banco de dados com as informações coletadas em promoção comercial, sendo expressamente vedada a comercialização ou a cessão, ainda que a título gratuito, desses dados.</p><br>
        <p><b>13. Impedidos de participarem da promoção.</b> Ficará vetada a participação de diretores, administradores e funcionários da PROMOTORA, da Agência 96, e escritório de advocacia Patrícia Nakano.</p><br>
        <p><b>14. Reconhecimento de cláusulas e aceitação das regras do Regulamento.</b> A simples participação na presente promoção implicará no total e integral reconhecimento das condições e aceitação irrestrita deste Regulamento, bem como, presumir-se-á a condição de que o participante ganhador não possui impedimentos fiscal, legal ou outro que o impeça de receber e/ou usufruir o prêmio ganho.</p><br>
    </div>
</main><!-- /.container -->



<?php
include 'Rodape.php';

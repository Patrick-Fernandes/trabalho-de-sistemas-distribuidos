<?php
session_start();
header("Content-type: text/html; charset=utf-8");
include '../Controler/conexao.php';
include '../Controler/Funcoes.php';
?>
<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Concurso Oscar Avenida </title>
        <link href="css/dashboard.css" rel="stylesheet">
        <link href="css/album.css" rel="stylesheet" type="text/css"/>
        <link href="css/blog.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <!-- Principal CSS do Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Estilos customizados para esse template -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/locastyle.css" rel="stylesheet" type="text/css"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://www.geradorcpf.com/assets/js/jquery.maskedinput-1.1.4.pack.js"></script>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/locastyle.js" type="text/javascript"></script>

    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

            <?php
            if (isset($_SESSION['usuarioId'])) {
                $_SESSION['usuarioId'];
                $_SESSION['usuarioNome'];
                $_SESSION['usuarioNiveisAcessoId'];
                $_SESSION['usuarioEmail'];
                ?> <a class="navbar-brand" href="Sair">Sair

                </a> <?php
        } else {
                ?> <a class="navbar-brand" href="Login">Login

                </a> <?php
        }
            ?>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <?php
                    if (isset($_SESSION['usuarioId'])) {
                        $_SESSION['usuarioId'];

                        $_SESSION['usuarioNiveisAcessoId'];
                        $_SESSION['usuarioEmail'];
                        ?>   <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <?= $_SESSION['usuarioNome']; ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="Perfil">Perfil</a>
                                <a class="dropdown-item" href="Meus-Votos">Meus Votos</a>
                                <!--<a class="dropdown-item" href="#">Algum outro item</a>-->
                            </div>
                        </li>
                        <?php
                    } else {
                        
                    }
                    ?>
                    <li class="nav-item ">
                        <a class="nav-link" href="Home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Filmes">Votar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="Premios">Prêmios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="Regulamento">Regulamento</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../Cinema-Avenida/">Cinemas Avenida</a>
                    </li>   <li class="nav-item">
                        <a class="nav-link " href=""></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="Editar-Perfil"> <?php
                    if (isset($_SESSION['usuarioId'])) {
                        if (verificaCadadastro($_SESSION['usuarioId']) == 'erro') {
                            ?>
                                    <SPAN style="color: #fff;background-color: red; margin-left: 200px ">
                                        COMPLETE SEU CADASTRO PARA PODER PARTICIPAR 
                                    </sPan>
        <?php
    }
}
?></a>
                    </li>


                </ul>

            </div>

        </nav>


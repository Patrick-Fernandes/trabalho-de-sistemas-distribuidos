<?php session_start(); 
include '../Controler/Funcoes.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="generator" content="">
        <title>Cadastro</title>
        <!-- Bootstrap core CSS -->
        <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://getbootstrap.com/docs/4.3/examples/sign-in/signin.css" rel="stylesheet">

        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <meta name="google-signin-client_id" content="616809964020-7ad7nvioj1amnvseurae2qq558e7b1b7.apps.googleusercontent.com">
        <link href="css/dashboard.css" rel="stylesheet">
        <link href="css/album.css" rel="stylesheet" type="text/css"/>
        <link href="css/blog.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <!-- Principal CSS do Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Estilos customizados para esse template -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/locastyle.css" rel="stylesheet" type="text/css"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://www.geradorcpf.com/assets/js/jquery.maskedinput-1.1.4.pack.js"></script>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/locastyle.js" type="text/javascript"></script>
        <!-- Custom styles for this template -->
    </head>
    <body class="text-center">
        <script type="text/javascript">
            $(document).ready(function () {
                $("#cpf").mask("999.999.999-99");
            });
            $(document).ready(function () {
                $("#tel").mask("(99) 99999-9999");
            });
            $(document).ready(function () {
                $("#tel2").mask("(99) 99999-9999");
            });
            $(document).ready(function () {
                $("#data").mask("99/99/9999");
            });
            $(document).ready(function () {
                $("#cep").mask("99999-999");
            });


        </script>
    <div style="align-content: center; margin: 0 auto">

        <form action="cadastro-envia" method="post" class="form-check center" >
            <fieldset>
                <h1 class="h3 mb-12 font-weight-normal">Faça seu cadastro 
                </h1>
                <h5>  <?php
                    if (isset($_SESSION['loginErro'])) {
                        ?> <?= $_SESSION['loginErro'] ?><?php
                    }
                    ?></h5>
                <label for="txUsuario" for="inputEmail" class="sr-only">E-mail</label>
              

                <p style="color: red">* campos obrigatorios</p>
                <!-- Approach -->
                <h4 class="m-0 font-weight-bold text-primary"> Dados basicos:</h4>

                <b>Nome de Login: </b>  
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='nome' value=''>
                <b>Email:  </b>         
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='email' value=''>
                <br><br>
                <b>Senha:  </b>             
                <span style="color: red"><b>*</b></span>
                <input   required type='password' name='senha' value=''>
                <b>Confirmar senha:  </b>    
                <span style="color: red"><b>*</b></span>
                <input   required type='password' name='senha2' value=''>
                <!-- Approach -->
                <h4 class=" text-primary"> Dados avançados:</h4><br>
                <p>Preencher esses dados corretamente faz você poder concorrer aos prêmios</p><br>
                <b>Nome Completo: </b>
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='NomeCompleto' value=''>
                <b>CPF:  </b>
                <span style="color: red"><b>*</b></span>
                <input   required type='text' 
                        onblur=" TestaCPF(this.value);"
                        name='cpf' id="cpf" value=''><br><br>
                <b> Telefone: </b>
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='tel' id='tel' value=''>
                <b> Telefone 2: </b>
                <input    type='text' name='tel2' id='tel2' value=''><br><br>
                <b> Data de Nascimento </b>
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='data' id="data" value=''>
                <b>CEP:  </b><a href="http://www.buscacep.correios.com.br/sistemas/buscacep/">não sei cep</a>
                <span style="color: red"><b>*</b></span>
                <input   required name="cep" type="text" id="cep" size="10" maxlength="9"
                        onblur="pesquisacep(this.value);"  value=''><br><br>
                <label><b>Rua:                            </b>
                    <span style="color: red"><b>*</b></span>
                    <input   required name="rua" type="text" id="rua" size="60" value='' />
                </label><br />
                <b>Nº  </b>
                <span style="color: red"><b>*</b></span>
                <input   required type='text' name='numero' value=''>
                <label>Bairro:
                    <span style="color: red"><b>*</b></span>
                    <input   required name="bairro" type="text" id="bairro" size="40" value=''> 
                </label><br />
                <b>Complemento:  </b>
                <input    type='text' name='complemento' value=''>

                <label>Cidade:
                    <span style="color: red"><b>*</b></span>
                    <input   required name="cidade" type="text" id="cidade" size="40"  value='' />
                </label><br />
                <label>Estado:
                    <span style="color: red"><b>*</b></span>
                    <input   required name="uf" type="text" id="uf" size="2" value='' />
                </label><br />

                <input   required name="ibge" type="hidden" id="ibge" size="8" /></label><br />




            <p><input   type="checkbox" value="Cadastrar"lass="btn " required="" />
                Li e concordo com os termos do <a href="regulamento">regulamento</a></p>
            <br>


            <input   type="submit" value="Cadastrar" class="btn  btn-primary " />
            <br>
            <br>       
            <p><a href="Login" class="btn btn-primary" style="color: #fff">Logue-se</a></p>


            <br>
            <p class="mt-5 mb-3 text-muted" id="msg">&copy; 2017-2019</p>
        </fieldset>

    </form>
        </div>
    <!--google informações-->




    <!--google fim-->


</body>
</html>
<?php
$_SESSION['loginErro'] = '';

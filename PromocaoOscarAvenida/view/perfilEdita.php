<?php
include './Cabecalho.php';
//verifica se ta logado
if (!isset($_SESSION['usuarioId'])) {
    echo '<script>location.href = "login";</script>';
}

//busca dados do usuario
$sqlBC = " SELECT u.`id`, u.`nome`,  u.`email`,  u.`id_google` ,
 dc.`cpf`, dc.`dataNascimento`, dc.`telefone`, dc.`telefone2`,
 dc.`cep`, dc.`endereco`, dc.`numero`, dc.`complemento`,
 dc.`bairro`, dc.`estado`, dc.`cidade`,dc.`NomeCompleto`
 FROM `usuario` u
left join  dadoscadastrais dc on u.id = dc.id_usuario
WHERE u.id = " . $_SESSION['usuarioId'];
$resultBC = mysqli_query($conn, $sqlBC);
if (mysqli_num_rows($resultBC) > 0) {
    $rowBC2 = mysqli_fetch_assoc($resultBC);
    ?>
    <!--                    mascaras
                     https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublica.asp
                     
                     https://www.geradorcpf.com/mascara-cpf-com-jquery.htm-->

    <script type="text/javascript">
        $(document).ready(function () {
            $("#cpf").mask("999.999.999-99");
        });
        $(document).ready(function () {
            $("#tel").mask("(99) 99999-9999");
        });
        $(document).ready(function () {
            $("#tel2").mask("(99) 99999-9999");
        });
        $(document).ready(function () {
            $("#data").mask("99/99/9999");
        });
        $(document).ready(function () {
            $("#cep").mask("99999-999");
        });


    </script>
    <style>
        p{
            font-size: 15px;
        }
    </style>
    <main role="main" class="container">
        <div class="starter-template">
            <h1>Perfil de <?= $_SESSION['usuarioNome'] ?> </h1>
            <p style="color: red">* campos obrigatorios</p>
            <div class="col-lg-12 mb-4">
                <!-- Approach -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-primary"> Dados basicos:</h4>
                    </div>
                    <div class="card-body">
                        <form  action="PerfilUsuarioControler" method="post">

                            <input type="hidden" name="idUser" value="<?= $_SESSION['usuarioId'] ?>">
                            <p class="mb-0"><b>Nome de Login: </b>  
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='nome' value='<?= $rowBC2['nome']; ?>'> </p>
                            <p class="mb-0"><b>Email:  </b>         
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='email' value='<?= $rowBC2['email']; ?>'> </p>
                            <p class="mb-0"><b>Senha:  </b>             
                                <span style="color: red"><b>*</b></span>
                                <input required type='password' name='senha' value=''> </p>
                            <p class="mb-0"><b>Confirmar senha:  </b>    
                                <span style="color: red"><b>*</b></span>
                                <input required type='password' name='senha2' value=''> </p>

                            <p>
                                <input required type="submit" class="btn btn-block btn-facebook" value="Editar Perfil">
                            </p>
                        </form>


                    </div>
                </div>
                <!-- Approach -->

                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-primary"> Dados avançados:</h4>
                    </div>

                    <div class="card-body">
                        <form action="DadosCadastriasUsuarioControler" method="POST">
                            <input type="hidden" name="idUser" value="<?= $_SESSION['usuarioId'] ?>">
                            <p class="mb-0"><b>Nome Completo: </b>
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='NomeCompleto' value='<?= $rowBC2['NomeCompleto']; ?>'> </p>
                            <p class="mb-0"><b>CPF:  </b>
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' 
                                       onblur=" TestaCPF(this.value);"
                                       name='cpf' id="cpf" value='<?= $rowBC2['cpf']; ?>'> </p>
                            <p class="mb-0"><b> Telefone: </b>
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='tel' id='tel' value='<?= $rowBC2['telefone']; ?>'> </p>
                            <p class="mb-0"><b> Telefone 2: </b>
                                <input  type='text' name='tel2' id='tel2' value='<?= $rowBC2['telefone2']; ?>'> </p>
                            <p class="mb-0"><b> Data de Nascimento </b>
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='data' id="data" value='<?= $rowBC2['dataNascimento']; ?>'> </p>
                            <p class="mb-0"><b>CEP:  </b>
                                <span style="color: red"><b>*</b></span>
                                <input required name="cep" type="text" id="cep" size="10" maxlength="9"
                                       onblur="pesquisacep(this.value);"  value='<?= $rowBC2['cep']; ?>'> 
                                <a target="_blank" href="http://www.buscacep.correios.com.br/sistemas/buscacep/">não sei cep</a>
                            </p>
                            <label><b>Rua:                            </b>
                                <span style="color: red"><b>*</b></span>
                                <input required name="rua" type="text" id="rua" size="60" value='<?= $rowBC2['endereco']; ?>' />
                            </label><br />
                            <p class="mb-0"><b>Nº  </b>
                                <span style="color: red"><b>*</b></span>
                                <input required type='text' name='numero' value='<?= $rowBC2['numero']; ?>'> </p>
                            <label>Bairro:
                                <span style="color: red"><b>*</b></span>
                                <input required name="bairro" type="text" id="bairro" size="40" value='<?= $rowBC2['bairro']; ?>'> 
                            </label><br />
                            <p class="mb-0"><b>Complemento:  </b>
                                <input  type='text' name='complemento' value='<?= $rowBC2['complemento']; ?>'> </p>

                            <label>Cidade:
                                <span style="color: red"><b>*</b></span>
                                <input required name="cidade" type="text" id="cidade" size="40"  value='<?= $rowBC2['cidade']; ?>' />
                            </label><br />
                            <label>Estado:
                                <span style="color: red"><b>*</b></span>
                                <input required name="uf" type="text" id="uf" size="2" value='<?= $rowBC2['estado']; ?>' />
                            </label><br />

                            <input required name="ibge" type="hidden" id="ibge" size="8" /></label><br />



                            <input required type="submit" class="btn btn-block btn-facebook" value="Editar Dados Cadastrais">

                        </form>

                    </div>
                </div>
            </div>
            <div class="row">
            </div>


        </div>
    </main><!-- /.container -->



    <?php
}
include 'Rodape.php';

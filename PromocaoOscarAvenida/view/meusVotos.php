<?php include './Cabecalho.php'; ?>



<main role="main" class="container">

    <div class="starter-template">
        <h1>Filmes em que você votou</h1>

        <div class="container">
            <div class="card-deck mb-3 text-center">
                <?php
                $resultFILME = filmesQVotei($_SESSION['usuarioId']);
                while ($rowFILME2 = mysqli_fetch_assoc($resultFILME)) {
                    ?> 
                    <div class="card mb-4 shadow-sm">
                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal">melhor <?= $rowFILME2["tipoVoto"] ?></h4>
                        </div>
                        <p><b>Mensagem que escreveu:</b><?= $rowFILME2["mensagem"] ?> </p>
                        <div class="card-body">
                            <h1 class="card-title pricing-card-title">
                                <img height="150px" width="100px" src="<?= $rowFILME2["LinkImgFilme"] ?>">
                            </h1>
                            <ul class="list-unstyled mt-3 mb-4">
                                <li><b>Nome: </b><?= $rowFILME2["NomeFilme"] ?></li>
                                <li><b>Gênero</b><?= $rowFILME2["GeneroFilme"] ?></li>

                            </ul>
                            <a href="Apaga-VOTO-<?= $rowFILME2["id"] ?>" 
                               class="btn btn-lg btn-block btn-outline-primary btn-danger">APAGAR VOTO?</a>
                        </div>
                    </div>
                    <?php
                }
                ?>


            </div>

        </div>
</main><!-- /.container -->



<?php
include 'Rodape.php';

<?php
include './Cabecalho.php';
include './TopoConcurso.php';

if ($count > 0) {
    while ($count != $i) {
//  ``, ``, ``, ``, ``, ``, `linkImg` 
        $id = $itens["$i"]['id'];
        $nome = $itens["$i"]['nome'];
        $descricao = $itens["$i"]['descricao'];
        $genero = $itens["$i"]['genero'];
        $duracao = $itens["$i"]['duracao'];
        $img = $itens["$i"]['linkImg'];
        ?>             
        <div class='col-md-3'>
            <div class='card mb-4 shadow-sm'>
                <h6><?= $nome ?></h6>
                <img class='card-img-top'width="100" height="250" src='<?= $img ?>' alt='<?= $nome ?>'>
                <div class='card-body'>
                    <p class='card-text'><?= substr("$descricao", 0, 90) ?> 
                        <br> <a href="Filme-<?= $id ?>">ver mais</a> </p>
                    <p class='card-text btn-primary' style="color: #CCCCCC"> 
                        <?= contaVotosFilmes($id, $idVoto); ?> 
                    </p>


                    <div class='d-flex justify-content-between align-items-center'>
                        <small class='text-muted'><?= $duracao ?> min</small><br>
                        <small class='text-muted'>Gênero <?= $genero ?></small>
                    </div>
                    <div class='btn-group col-md-12'>
                        <?php if (!isset($_SESSION['usuarioId'])) { ?>
                            <button onclick="loguePorFavor()()" type='button' 
                                    class='btn btn-sm btn-primary'>Votar</button>
                                    <?php
                                } else if (verificaSeJaVotou($_SESSION['usuarioId'], $idVoto) == true) {

                                    $sqlVVJV = "   SELECT `idFilme` ,voto.id as votooo
                  FROM `voto` WHERE `user_id` = " . $_SESSION['usuarioId'] . "
                           and `tipo_id` = $idVoto";
                                    $resultVVJV = mysqli_query($conn, $sqlVVJV);
                                    echo 'Você já votou nesta catégoria <Br>';

                                    if (mysqli_num_rows($resultVVJV) > 0) {
                                        while ($rowVVJV = mysqli_fetch_assoc($resultVVJV)) {
                                            if ($rowVVJV['idFilme'] == $id) {
                                                ?><br>
                                                <a href="Apaga-VOTO-<?= $rowVVJV['votooo']?>" type='button' 
                                                class='btn btn-sm btn-danger'>Deseja apagar <br>seu voto?</a>

                                        <?php
                                    }
                                }
                            } else {
                                
                            }
                        } else if ($pen == 1) {
                            ?>

                            <form name="voto" action="Votar" method="POST">
                                <input type="hidden" name="user_id" value="<?= $_SESSION['usuarioId'] ?>" />

                                <input type="hidden" name="tipo_id" value="<?php
                                $sqlBC = "SELECT * FROM `tipo` where nome = '" . colocaAcento(limpaURL($_SERVER["REQUEST_URI"])) . "'";
                                $resultBC = mysqli_query($conn, $sqlBC);
                                if (mysqli_num_rows($resultBC) > 0) {
                                    // output data of each row
                                    while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
                                        echo $rowBC2["id"];
                                    }
                                } else {
                                    echo "null";
                                }
                                ?>" />
                                <input type="hidden" name="NomeFilme" value="<?= $nome ?>" />
                                <input type="hidden" name="LinkImgFilme" value="<?= $img ?>" />
                                <input type="hidden" name="GeneroFilme" value="<?= $genero ?>" />
                                <input type="hidden" name="idFilme" value="<?= $id ?>" />
                                <textarea name="mensagem" rows="6" cols="19" placeholder="pode deixar em branco"></textarea>

                                <input type="submit"   class='btn btn-sm btn-primary' value="Votar">

                            </form> 
                            <?php
                        } else {
                            ?>
                            <button onclick="categoria()" type='button' 
                                    class='btn btn-sm btn-primary'>Votar</button>
                        <?php } ?>

                    </div>
                    <br>
                </div>
            </div>
        </div>
        <?php
        $i++;
    }
}
?>
<?php
include './RodapeConcurso.php';

<?php include './Cabecalho.php'; ?>



    <main role="main" class="container">

      <div class="starter-template">
        <h1>Filmes que estão ganhando</h1>
        <p class="lead">
            
    <div class="row">
        <?php
        $resultFILME = topoVotos(1);
        $rowFILME2 = mysqli_fetch_assoc($resultFILME)
        ?> 
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">               
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Melhor <?= $rowFILME2["tipoVoto"] ?> </div>
                            <div class="text-xs font-weight-bold text-black-50 text-uppercase mb-1"><?= $rowFILME2["NomeFilme"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Votos <?= $rowFILME2["Qtd"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                <img src="<?= $rowFILME2["LinkImgFilme"] ?>"
                                     width="100" height="150">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <?php
        $resultFILME = topoVotos(2);
        $rowFILME2 = mysqli_fetch_assoc($resultFILME)
        ?> 
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">               
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Melhor <?= $rowFILME2["tipoVoto"] ?> </div>
                            <div class="text-xs font-weight-bold text-black-50 text-uppercase mb-1"><?= $rowFILME2["NomeFilme"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Votos <?= $rowFILME2["Qtd"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                <img src="<?= $rowFILME2["LinkImgFilme"] ?>"
                                     width="100" height="150">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
        $resultFILME = topoVotos(3);
        $rowFILME2 = mysqli_fetch_assoc($resultFILME)
        ?> 
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">               
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Melhor <?= $rowFILME2["tipoVoto"] ?> </div>
                            <div class="text-xs font-weight-bold text-black-50 text-uppercase mb-1"><?= $rowFILME2["NomeFilme"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Votos <?= $rowFILME2["Qtd"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                <img src="<?= $rowFILME2["LinkImgFilme"] ?>"
                                     width="100" height="150">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
        $resultFILME = topoVotos(4);
        $rowFILME2 = mysqli_fetch_assoc($resultFILME)
        ?> 
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">               
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Melhor <?= $rowFILME2["tipoVoto"] ?> </div>
                            <div class="text-xs font-weight-bold text-black-50 text-uppercase mb-1"><?= $rowFILME2["NomeFilme"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Votos <?= $rowFILME2["Qtd"] ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                <img src="<?= $rowFILME2["LinkImgFilme"] ?>"
                                     width="100" height="150">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
            
        </p>
      </div>
    </main><!-- /.container -->

   

<?php include 'Rodape.php';
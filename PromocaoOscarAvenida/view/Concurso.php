<?php
include './Cabecalho.php';
include './TopoConcurso.php';


if ($count > 0) {
    while ($count != $i) {
        //  ``, ``, ``, ``, ``, ``, `linkImg` 
        $id = $itens["$i"]['id'];
        $nome = $itens["$i"]['nome'];
        $descricao = $itens["$i"]['descricao'];
        $genero = $itens["$i"]['genero'];
        $duracao = $itens["$i"]['duracao'];
        $img = $itens["$i"]['linkImg'];
        ?>             
         <div class='col-md-3'>
            <div class='card mb-4 shadow-sm'>
                <h6><?= $nome ?></h6>
                <img class='card-img-top'width="100" height="250" src='<?= $img ?>' alt='<?= $nome ?>'>
                <div class='card-body'>
                    <p class='card-text'><?= substr("$descricao", 0, 90) ?> 
                        <br> <a href="Filme-<?= $id ?>">ver mais</a> </p>
                        <p class='card-text btn-primary' style="color: #CCCCCC"> 
                        Escolha uma catégoria
                    </p>
                    <div class='d-flex justify-content-between align-items-center'>
                        <small class='text-muted'><?= $duracao ?> min</small><br>
                        <small class='text-muted'>Gênero <?= $genero ?></small>
                    </div>
                    <div class='btn-group col-md-12'>
                        <form name="voto" action="Votar" method="POST">
                            <button onclick="categoria()" type='button' 
                                    class='btn btn-sm btn-primary'>Votar</button>
                        </form> 
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <?php
        $i++;
    }
}
?>
<?php
include './RodapeConcurso.php';

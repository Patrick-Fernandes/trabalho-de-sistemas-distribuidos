<?php include './Cabecalho.php'; ?>



<main role="main" class="container">

    <div class="starter-template">
        <h1>Lista das premiações do concurso</h1>
        <p>Não esqueça de ler o <a href="Regulamento">Regulamento</a></p>
        <p class="lead">
        <main role="main">

            <div class="container marketing">

                <!-- Três colunas de texto, abaixo do carousel -->
                <div class="row">
                    <div class="col-lg-3">
                        <h2>Melhor Filme</h2>
                        <p>Aquele que enviar a melhor história do por que aquele filme merece ganhar o prêmio de Melhor filme.</p>
                        <p>Ganhara além de 5 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca, camiseta, caneta. </p>
                        <p>Levará para casa um cheque de <BR> <span style="color: red; font-size: 1.5em">5 mil reais</span>  também.</p>
                    </div><!-- /.col-lg-4 -->

                    <div class="col-lg-3">
                        <h2>Melhor Trilha Sonora</h2>
                        <p>Aquele que enviar a melhor história do por que aquele filme merece ganhar o prêmio de Melhor filme.</p>
                        <p>Ganhara além de 3 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                        <p>Levará para casa um cheque de <BR> <span style="color: red; font-size: 1.5em">3 mil reais</span>   também.</p>
                    </div><!-- /.col-lg-4 -->



                    <div class="col-lg-3">
                        <h2>Melhor Direção</h2>
                        <p>Aquele que enviar a melhor história do por que aquele filme merece ganhar o prêmio de Melhor filme.</p>
                        <p>Ganhara além de 3 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                        <p>Levará para casa um cheque de <BR> <span style="color: red; font-size: 1.5em">3 mil reais</span>  também.</p>
                    </div><!-- /.col-lg-4 -->

                    <div class="col-lg-3">
                        <h2>Melhor Figurino</h2>
                        <p>Aquele que enviar a melhor história do por que aquele filme merece ganhar o prêmio de Melhor filme.</p>
                        <p>Ganhara além de 3 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                        <p>Levará para casa um cheque de <BR> <span style="color: red; font-size: 1.5em">3 mil reais</span>   também.</p>
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->


                <!-- COMEÇAM AS MENCIONADAS FEATUREZINHAS xD -->

                <hr class="featurette-divider">

                <div class="row featurette">
                    <div class="col-md-7">

                        <h2 class="featurette-heading">Todos que votaram no filme ganhador de
                            <span class="text-muted">Melhor Filme</span>  </h2>
                        <p class="lead">Ganhara 3 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano mais caneca e caneta. </p>
                    </div>
                </div>
                <hr class="featurette-divider">
                <!-- /FIM DAS FEATUREZINHAS *-* -->
       <div class="row featurette">
                    <div class="col-md-7">

                        <h2 class="featurette-heading">Todos que votaram no filme ganhador de
                            <span class="text-muted">Melhor  Trilha Sonora</span>  </h2>
                        <p class="lead">Ganhara 2 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                    </div>
                </div>
                <hr class="featurette-divider">
                <!-- /FIM DAS FEATUREZINHAS *-* -->  
          
                   <div class="row featurette">
                    <div class="col-md-7">

                        <h2 class="featurette-heading">Todos que votaram no filme ganhador de
                            <span class="text-muted">Melhor Direção</span>  </h2>
                        <p class="lead">Ganhara 2 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                    </div>
                </div>
                <hr class="featurette-divider">
                <!-- /FIM DAS FEATUREZINHAS *-* -->
                       <div class="row featurette">
                    <div class="col-md-7">

                        <h2 class="featurette-heading">Todos que votaram no filme ganhador de
                            <span class="text-muted">Melhor Figurino</span>  </h2>
                        <p class="lead">Ganhara 2 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                    </div>
                </div>
                <hr class="featurette-divider">
                <!-- /FIM DAS FEATUREZINHAS *-* --> 
                   <div class="row featurette">
                    <div class="col-md-7">

                        <h2 class="featurette-heading">Todos que votaram no filme ganhador de
                            <span class="text-muted">Melhor Filme</span>  </h2>
                        <p class="lead">Ganhara 2 voucher para assistir qualquer filme de qualquer um dos nossos cinemas, válidos por 1 ano, mais caneca e caneta. </p>
                    </div>
                </div>
                <hr class="featurette-divider">
                <!-- /FIM DAS FEATUREZINHAS *-* -->
            </div><!-- /.container -->

            </p>
    </div>
</main><!-- /.container -->



<?php
include 'Rodape.php';

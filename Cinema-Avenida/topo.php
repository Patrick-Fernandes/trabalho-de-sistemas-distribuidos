<?php
include './controler/conexao.php';
include './controler/funcao.php';

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="https://getbootstrap.com.br/docs/4.1//https://getbootstrap.com.br/docs/4.1//favicon.ico">

        <title>Cinemas Avenida</title>

        <!-- Principal CSS do Bootstrap -->
        <link href="https://getbootstrap.com.br/docs/4.1//dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Estilos customizados para esse template -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
        <link href="https://getbootstrap.com.br/docs/4.1/examples/blog/blog.css" rel="stylesheet">
    </head>

    <body>

        <div class="container">
            <header class="blog-header py-3">
                <div class="row flex-nowrap justify-content-between align-items-center">
                    <div class="col-4 pt-1">
                        <a class="text-muted" href="Home">Home</a>
                    </div>
                    <div class="col-4 text-center">
                        <a class="blog-header-logo text-dark" href="#">Cinemas Avenida</a>
                    </div>
                    <div class="col-4 d-flex justify-content-end align-items-center">

                    </div>
                </div>
            </header>

            <div class="nav-scroller py-1 mb-2 center align-items-center"  >
                <nav class="nav d-flex center" style="text-align: center; margin: 0 auto">
                    <div style="text-align: center; margin: 0 auto"> 
                        <?php
                        $sqlBC = "SELECT `id`, `nome` FROM `cinema`";
                        $resultBC = mysqli_query($conn, $sqlBC);
                        if (mysqli_num_rows($resultBC) > 0) {
                            // output data of each row
                            while ($rowBC2 = mysqli_fetch_assoc($resultBC)) {
                                ?>  <a class="p-2 text-muted" href="Cinema-<?= $rowBC2['id'];?>"><?= $rowBC2['nome'];?></a>
                                <?php
                            }
                        } else {
                            echo "Nenhum cinema existi";
                        }
                        ?>


                    </div>

                </nav>
            </div>
  <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Não deixe de participar do OSCAR AVENIDA</h1>
          <p class="lead my-3">Os filmes serão escolidos por 4 catégorias,  melhor filme, melhor trilha sonora, melhor direção, melhor figurino,  os mais votatos nessas catégorias, serão 
                prêmiados com ingreços para qualquer sessão de qualquer um dos nossos cinemas, e mais, e aquele que enviar 
                a melhor história do por que este filme merece ser o ganhador, levará para casa um cheque de 
                <span style="color: red; font-size: 1.5em">5 mil reais</span>, você não sairá de fora certo?.
                E não se esqueçam de ler o <a href="http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida/Regulamento">Regulamento</a>
                do concurso.</p>
          <p class="lead mb-0"><a href="http://localhost/trabalho-de-sistemas-distribuidos/PromocaoOscarAvenida" class="text-white font-weight-bold">Ir até o concurso...</a></p>
        </div>
      </div>
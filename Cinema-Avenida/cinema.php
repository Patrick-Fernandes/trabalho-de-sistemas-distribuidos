<?php
include './topo.php';
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
?>

<h1>Sessões do Cinema</h1>  

<div class="row mb-2">

    <?php
    $sqlS = "SELECT s.`id`, `sala`, `hora`, DATE_FORMAT(diaI, '%d/%m/%Y') as 'diaI', 
        DATE_FORMAT(diaF, '%d/%m/%Y') as 'diaF', `formato`, `filme_id`, `cinema_id` ,
c.nome as cinema,c.cidade,c.descricao,c.estado,
f.nome,f.descricao as sinopse, f.classificacao,f.genero,f.duracao,f.linkImg,f.ano

FROM `sessao` s
LEFT JOIN cinema c on s.cinema_id =  c.id
LEFT JOIN filme f on s.filme_id = f.id
where c.id =$id  
";
    $resultS = mysqli_query($conn, $sqlS);
    if (mysqli_num_rows($resultS) > 0) {
        // output data of each row
        while ($rowS2 = mysqli_fetch_assoc($resultS)) {
            ?>  
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-success"><?= $rowS2['nome']; ?></strong>

                        <div class="mb-1 text-muted">de <?= $rowS2['diaI']; ?> até <?= $rowS2['diaF']; ?><BR>
                            Sessão das <?= $rowS2['hora']; ?>, sala <?= $rowS2['sala']; ?>                    
                        </div>
                        <p class="card-text mb-auto">
                            
                            <img src="<?= classificacao($rowS2['classificacao']) ?>"  width="25" height="25">  
                            Gênero:   <?= $rowS2['genero']; ?>  
                            Duração:  <?= $rowS2['duracao']; ?> min
                        </p><br>
                        <p class="card-text mb-auto"> <?= $rowS2['sinopse']; ?></p>
                        <a href="https://www.ingresso.com">Compre o Ingreço </a>
                    </div>
                    <img class="card-img-right flex-auto d-none d-lg-block" 
                         src="<?= $rowS2['linkImg']; ?>" alt="Card image cap">
                </div>
            </div>
            <?php
        }
    } else {
        echo "nenhuma sessão encontrada";
    }
    ?>

</div>
<br><br><br><br><br><br>

<?php
$sqlCC = "SELECT 
c.nome as cinema,c.cidade,c.descricao,c.estado
FROM  cinema c 
where c.id =$id  
";
$resultCC = mysqli_query($conn, $sqlCC);

// output data of each row
$rowCC2 = mysqli_fetch_assoc($resultCC);
?> 
<main role="main" class="container">
    <div class="row">
        <div class="col-md-8 blog-main">
            <h3 class="pb-3 mb-4 font-italic border-bottom">

            </h3>

            <div class="blog-post">
                <h2 class="blog-post-title">Cine Avenida - <?= $rowCC2['cinema']; ?></h2>
                <p class="blog-post-meta"> <?= $rowCC2['cidade']; ?> , <?= $rowCC2['estado']; ?></p>

                <p>
                    <?= $rowCC2['descricao']; ?>
                </p>
            </div><!-- /.blog-post -->

            
<?php
include './rodape.php';

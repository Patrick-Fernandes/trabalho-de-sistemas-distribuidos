
<!doctype html>
<html lang="pt-br" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="https://getbootstrap.com.br/docs/4.1/https://getbootstrap.com.br/docs/4.1/favicon.ico">

        <title>Template para sticky footer e navbar fixa, usando Bootstrap.</title>

        <!-- Principal CSS do Bootstrap -->
        <link href="https://getbootstrap.com.br/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Estilos customizados para esse template -->
        <link href="https://getbootstrap.com.br/docs/4.1/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">
    </head>

    <body class="d-flex flex-column h-100">

        <header>
            <!-- Navbar fixa -->
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">


                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home">Voltar Para o Site <span class="sr-only">(atual)</span></a>
                        </li>

                    </ul>

                </div>
            </nav>
        </header>

        <!-- Começa o conteúdo da página -->
        <main role="main" class="flex-shrink-0">
            <div class="container">
                <h1 class="mt-5">Examplo URL</h1>
                <p class="lead">API de buscas dos filmes do cinema avenida.</p>
                <p>JSON: 
                    <a href="http://localhost/trabalho-de-sistemas-distribuidos/public/WS/WS_Filme.php">
                        http://localhost/trabalho-de-sistemas-distribuidos/public/WS/WS_Filme.php</a> 
                </p>
            </div>


            <div class="container">
                <h2 class="mt-5">Parametros</h2>
                <p class="lead">Os parâmetros habilitados que podem ser usadaos nessa API.</p>
                <p><B>ano</B> (numerico): neste parâmetro, faz a busca de filmes especificos no ano colocado.</p>
                <p><B>genero</B> (String): neste parâmetro, faz a busca de filmes especificos no gênero colocado. </p>
                <p><B>id</B> (numerico): neste parâmetro, faz a busca de 1 filme especifico correspondente aquela ID </p>
                <p><B>nome</B> (String): neste parâmetro, faz a busca de filmes especificos que contenham todo 
                    ou parte do nome cidado neste parâmetro. </p>
                <p><B>classificacao</B> (String): neste parâmetro, faz a busca de filmes especificos na classificação indicada neste parâmetro.
                    <BR>
                    Abaixo lista das classificações disponiveis.
                <H5>Lista de Classificações disponiveis</H5>
                <ol type="1">
                    <li>L</li>
                    <li>10</li>
                    <li>12</li>
                    <li>14</li>
                    <li>16</li>
                    <li>18</li>
                    <li>ER</li>
                </ol>

                
                </p>
                <h4>Exemplo de URL com parâmetros:</h4>
                <p><br>
                    <a href="http://localhost/trabalho-de-sistemas-distribuidos/public/WS/WS_Filme.php?ano=2018">http://localhost/trabalho-de-sistemas-distribuidos/public/WS/WS_Filme.php?ano=2018</a>
                    <br>
                    Neste exemplo foi usado o exemplo do ano, buscando filmes somente do ano de 2018.
                </p>
            </div>





        </main>






        <footer class="footer mt-auto py-3">
            <div class="container">
            </div>
        </footer>

        <!-- Principal JavaScript do Bootstrap
        ================================================== -->
        <!-- Foi colocado no final para a página carregar mais rápido -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="https://getbootstrap.com.br/docs/4.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
        <script src="https://getbootstrap.com.br/docs/4.1/assets/js/vendor/popper.min.js"></script>
        <script src="https://getbootstrap.com.br/docs/4.1/dist/js/bootstrap.min.js"></script>
    </body>
</html>

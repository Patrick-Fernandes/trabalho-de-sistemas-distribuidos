<?php

function classificacao($i) {
    if ($i == 'l' || $i == 'L') {
        return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/DJCTQ_-_L.svg/75px-DJCTQ_-_L.svg.png";
    } else if ($i == '10') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/DJCTQ_-_10.svg/75px-DJCTQ_-_10.svg.png';
    } else if ($i == '12') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/DJCTQ_-_12.svg/75px-DJCTQ_-_12.svg.png';
    } else if ($i == '14') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/DJCTQ_-_14.svg/75px-DJCTQ_-_14.svg.png';
    } else if ($i == '16') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/DJCTQ_-_16.svg/75px-DJCTQ_-_16.svg.png';
    } else if ($i == '18') {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/DJCTQ_-_18.svg/75px-DJCTQ_-_18.svg.png';
    } else {
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/DJCTQ_-_ER.svg/75px-DJCTQ_-_ER.svg.png';
    }
}

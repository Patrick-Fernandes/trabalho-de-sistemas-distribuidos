<div class="blog-post">
                <h2 class="blog-post-title">Nossa Tabela Preços</h2>
                <img src="img/c1.png" alt=""/>
                <img src="img/c2.png" alt=""/>
                <img src="img/c3.png" alt=""/>
                <img src="img/c4.png" alt=""/>

                <p>
                <p class="blog-post-meta">
                
Nos feriados o preço da entrada corresponde ao valor promocional do dia da semana.
<br>
(*) Jovens até 15 anos; Estudantes; Professores; Idosos (acima de 60 anos); Portadores de deficiência. 
                </p>


                </p>
            </div><!-- /.blog-post -->



        </div><!-- /.blog-main -->


    </div><!-- /.row -->

</main><!-- /.container -->


    <!-- Principal JavaScript do Bootstrap
    ================================================== -->
    <!-- Foi colocado no final para a página carregar mais rápido -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com.br/docs/4.1//assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com.br/docs/4.1//assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com.br/docs/4.1//dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com.br/docs/4.1//assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>

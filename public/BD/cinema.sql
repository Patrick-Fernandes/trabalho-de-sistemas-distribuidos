-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Abr-2019 às 22:10
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cinema`
--

CREATE TABLE `cinema` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `descricao` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cinema`
--

INSERT INTO `cinema` (`id`, `nome`, `cidade`, `estado`, `descricao`) VALUES
(1, 'Shopping Pelotas', 'Pelotas', 'Rio Grande do Sul', NULL),
(2, 'Pelotas', 'Pelotas', 'Rio Grande do Sul', NULL),
(3, 'Kinoplex Avenida\r\n', 'Rio de Janeiro', 'RJ', '<b>Endereço:</b> Avenida 28 de março, 574 - Parque Leopoldina\r\n<br>\r\n<b>Telefone 1 </b><br>\r\n(22) 2665-9810'),
(4, 'Cine Araújo Maringá', 'Maringá', 'PR', '<b>Endereço:</b>Av. São Paulo, 458 - Centro\r\n<br>\r\n<b>Telefone 1 </b><br>\r\n\r\n(44) 3222-8080');

-- --------------------------------------------------------

--
-- Estrutura da tabela `filme`
--

CREATE TABLE `filme` (
  `id` int(8) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` text NOT NULL,
  `classificacao` varchar(2) NOT NULL,
  `genero` varchar(40) NOT NULL,
  `duracao` int(4) NOT NULL,
  `linkImg` text NOT NULL,
  `ano` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `filme`
--

INSERT INTO `filme` (`id`, `nome`, `descricao`, `classificacao`, `genero`, `duracao`, `linkImg`, `ano`) VALUES
(1, 'Shazam', 'Billy Batson tem apenas 14 anos de idade, mas recebeu de um antigo mago o dom de se transformar em um super-herói adulto chamado Shazam. Ao gritar a palavra SHAZAM!, o adolescente se transforma nessa sua poderosa versão adulta para se divertir e testar suas habilidades. Contudo, ele precisa aprender a controlar seus poderes para enfrentar o malvado Dr. Thaddeus Sivana', '12', 'ficção científica', 132, 'http://t3.gstatic.com/images?q=tbn:ANd9GcT1OECXx3Zjx-jSneVZMTR958ao4aBKY6VWUquEhTnT4JZApPkh', 2019),
(3, 'AFTER', '--', '14', 'drama', 85, 'https://static.wixstatic.com/media/f25496_e9180c927d814d42b2713b973cd7bf12~mv2.jpg/v1/crop/x_0,y_14,w_648,h_932/fill/w_155,h_223,al_c,q_80,usm_0.66_1.00_0.01/after.webp', 2019),
(4, 'DUMBO', '--', '10', 'aventura', 112, 'https://static.wixstatic.com/media/f25496_5ce1cf899ac0471599145b940e1779ab~mv2.jpg/v1/crop/x_3,y_0,w_765,h_1100/fill/w_155,h_223,al_c,q_80,usm_0.66_1.00_0.01/4386.webp', 2019),
(5, 'CAPITÃ MARVEL', '--', '12', 'ação', 128, 'https://static.wixstatic.com/media/f25496_c9ad00cbec294f26a319a120d3b3e6dd~mv2.jpg/v1/crop/x_2,y_0,w_695,h_1000/fill/w_155,h_223,al_c,q_80,usm_0.66_1.00_0.01/CAPIT%C3%83%20MARVEL.webp', 2019),
(6, 'DE PERNAS PRO AR 3', '--', '14', 'comédia', 100, 'https://static.wixstatic.com/media/f25496_3c100937fd294094a55055480d345fc5~mv2.jpg/v1/crop/x_0,y_10,w_653,h_939/fill/w_155,h_223,al_c,q_80,usm_0.66_1.00_0.01/DE%20PERNAS%20PARA%20O%20AR%203.webp', 2019),
(7, 'O Predador', 'Uma perseguição entre naves alienígenas traz à Terra um novo predador, que acaba sendo capturado por humanos. Antes disso, ele tem seu capacete e bracelete roubados por Quinn McKenna (Boyd Holbrook), um atirador de elite que estava em missão no local onde a nave caiu. A bióloga Casey Brackett (Olivia Munn) é então chamada para examinar o ser recém-descoberto, mas ele logo consegue escapar do laboratório em que é mantido cativeiro. Ao tentar recapturá-lo Casey encontra McKenna, que está em um ônibus repleto de ex-militares com problemas. Juntos, eles buscam um meio de sobreviver e, ao mesmo tempo, proteger o pequeno Rory (Jacob Tremblay), filho de McKenna, que está com os artefatos alienígenas pegos pelo pai.', '18', 'Ficção científica', 107, 'https://i0.wp.com/pipocamoderna.com.br/wp-content/uploads/2018/09/thepredator.jpg', 2018),
(8, 'Pantera Negra', 'Pantera Negra segue T\'Challa quando, depois dos eventos de Capitão América: Guerra Civil, ele retorna para a isolada e avançada nação africana de Wakanda, para tomar seu lugar como rei. Porém, quando um velho inimigo do passado aparece, o talento de T\'Challa como rei e como Pantera Negra é testado e ele entra em um conflito que coloca o destino de Wakanda e do mundo em risco', '10', 'Ação', 134, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1517600986.jpg', 2018),
(9, 'VINGADORES: GUERRA INFINITA', 'Em uma jornada cinematográfica sem precedentes que está sendo elaborada há dez anos e abrange todo o Universo Cinematográfico Marvel, \"Vingadores: Guerra Infinita\", da Marvel Studios, leva às telas o maior e mais mortal confronto de todos os tempos. Os Vingadores e seus aliados Super Heróis devem se dispor a sacrificar tudo em uma tentativa de derrotar o poderoso Thanos antes que seu ataque de devastação e ruina dê um fim ao universo.\r\n\r\n', '12', 'Ação', 156, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521581139.jpg', 2018),
(10, 'Os Incríveis 2', 'Helena cria uma petição para a volta dos super-heróis, enquanto Beto vive um dia a dia cuidando dos três filhos do casal, Violeta, Flecha e Zezé. É quando os superpoderes do bebê começam a ser descobertos. A missão deles sofre uma reviravolta quando um novo vilão surge com um brilhante e perigoso plano que ameaça acabar com o mundo. A família Pêra aceita o desafio e conta com a ajuda de Gelado para derrotar o vilão.\r\n\r\n', '10', 'animação', 118, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1523561406.jpg', 2018),
(11, 'Ilha de Cachorros', 'Atari Kobayashi é um garoto japonês de 12 anos de idade. Ele mora na cidade de Megasaki, sob tutela do corrupto prefeito Kobayashi. O político aprova uma nova lei que proíbe os cachorros de morarem no local, fazendo com que todos os animais sejam enviados a uma ilha vizinha repleta de lixo. Mas o pequeno Atari não aceita se separar do cachorro Spots. Ele convoca os amigos, rouba um jato em miniatura e parte em busca de seu fiel amigo. A aventura vai transformar completamente a vida da cidade.\r\n', '12', 'Drama', 105, 'http://t1.gstatic.com/images?q=tbn:ANd9GcT7dFCBWlACXboio9iKbEG4C01hLSmRdRV_fMcexJMsEKBrTmIp', 2018),
(12, 'HAN SOLO - UMA HISTÓRIA STAR WARS', 'Conheça o início das aventuras do emblemático mercenário Han Solo (Alden Ehrenreich) e seu fiel companheiro Chewbacca (Joonas Suotamo) antes dos eventos retratados em Star Wars: Uma Nova Esperança, inclusive como conheceram Lando Calrissian (Donald Glover) e conseguiram sua Millenium Falcon.\r\n\r\n', '12', 'aventura', 135, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/17/260x365_1524347971.jpg', 2018),
(13, 'O PRIMEIRO HOMEM', 'A vida do astronauta norte-americano Neil Armstrong (Ryan Gosling) e sua jornada para se tornar o primeiro homem a andar na Lua. Os sacrifícios e custos de Neil e toda uma nação durante uma das mais perigosas missões na história das viagens espaciais.\r\n', '12', 'drama', 142, 'http://br.web.img3.acsta.net/c_215_290/pictures/18/06/11/17/46/3758033.jpg', 2018),
(14, 'BOHEMIAN RHAPSODY', 'Freddie Mercury (Rami Malek) e seus companheiros Brian May (Gwilyn Lee), Roger Taylor (Ben Hardy) e John Deacon (Joseph Mazzello) mudam o mundo da música para sempre ao formar a banda Queen, durante a década de 1970. Porém, quando o estilo de vida extravagante de Mercury começa a sair do controle, a banda tem que enfrentar o desafio de conciliar a fama e o sucesso com suas vidas pessoais cada vez mais complicadas.\r\n', '14', 'drama', 142, 'http://br.web.img3.acsta.net/c_215_290/pictures/18/08/24/21/29/2341995.jpg', 2018),
(15, 'JOGADOR Nº1', 'O filme é ambientado no ano 2045, com o mundo à beira do caos e do colapso. Contudo, as pessoas encontraram refúgio no OASIS, um amplo universo de realidade virtual criado pelo genial e excêntrico James Halliday (Mark Rylance). Quando um jovem e improvável herói chamado Wade Watts (Tye Sheridan) decide participar da competição, ele entra em uma caça ao tesouro arriscada e capaz de distorcer a realidade, através de um universo fantástico de mistério, descoberta e perigo.\r\n\r\n', '12', 'Ficção Científica', 140, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521578990.jpg', 2018),
(16, 'ANIMAIS FANTÁSTICOS: OS CRIMES DE GRINDELWALD', 'No final do primeiro filme, o poderoso Bruxo das Trevas Gerardo Grindelwald (Johnny Depp) foi capturado pela MACUSA (Congresso Mágico dos Estados Unidos da América) com a ajuda de Newt Scamander (Eddie Redmayne). Mas, cumprindo sua ameaça, Grindelwald escapou e começou a reunir seguidores, que em sua maioria desconhecem seu verdadeiro propósito: elevar os bruxos de sangue puro para dominar todos os seres não-mágicos. Em um esforço para frustrar os planos de Grindelwald, AlvoDumbledore (Jude Law) procura seu antigo aluno, Newt Scamander, que concorda em ajudar, sem saber dos perigos que o aguardam. Linhas são traçadas quando amor e lealdade são testados, mesmo entre os amigos e familiares mais verdadeiros, em um mundo bruxo cada vez mais dividido.\r\n\r\n', '12', 'Aventura', 134, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1532959780.jpg', 2018),
(17, 'ANIQUILAÇÃO', 'Depois que seu marido desaparece em uma missão secreta, a bióloga Lena (Natalie Portman) embarca numa expedição a uma misteriosa região interditada pelo governo dos EUA.\r\n\r\n', '12', 'Ficção Científica', 115, 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521491919.jpg', 2018);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessao`
--

CREATE TABLE `sessao` (
  `id` int(11) NOT NULL,
  `sala` varchar(3) NOT NULL,
  `hora` time NOT NULL,
  `diaI` date NOT NULL,
  `diaF` date NOT NULL,
  `formato` varchar(40) NOT NULL,
  `filme_id` int(11) DEFAULT NULL,
  `cinema_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sessao`
--

INSERT INTO `sessao` (`id`, `sala`, `hora`, `diaI`, `diaF`, `formato`, `filme_id`, `cinema_id`) VALUES
(1, '3', '14:00:00', '2019-04-16', '2019-04-24', 'dublado', 1, 2),
(2, '3', '14:00:00', '2019-04-16', '2019-04-24', 'dublado', 1, 1),
(3, '3', '14:00:00', '2019-04-16', '2019-04-24', 'dublado', 5, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cinema`
--
ALTER TABLE `cinema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filme`
--
ALTER TABLE `filme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessao`
--
ALTER TABLE `sessao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filme_id` (`filme_id`),
  ADD KEY `cinema_id` (`cinema_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cinema`
--
ALTER TABLE `cinema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `filme`
--
ALTER TABLE `filme`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sessao`
--
ALTER TABLE `sessao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `sessao`
--
ALTER TABLE `sessao`
  ADD CONSTRAINT `sessao_ibfk_1` FOREIGN KEY (`filme_id`) REFERENCES `filme` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `sessao_ibfk_2` FOREIGN KEY (`cinema_id`) REFERENCES `cinema` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

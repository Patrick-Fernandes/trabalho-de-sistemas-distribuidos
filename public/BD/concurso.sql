-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Abr-2019 às 22:10
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `concurso`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dadoscadastrais`
--

CREATE TABLE `dadoscadastrais` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cpf` varchar(20) NOT NULL DEFAULT '000.000.000-00',
  `dataNascimento` varchar(10) NOT NULL DEFAULT '00/00/0000',
  `telefone` varchar(30) NOT NULL DEFAULT '(00) 00000-0000',
  `telefone2` varchar(30) NOT NULL DEFAULT '(00) 00000-0000',
  `cep` varchar(10) NOT NULL DEFAULT '00000-000',
  `endereco` varchar(220) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(120) DEFAULT NULL,
  `bairro` varchar(220) DEFAULT NULL,
  `estado` varchar(220) DEFAULT NULL,
  `cidade` varchar(220) DEFAULT NULL,
  `NomeCompleto` varchar(220) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dadoscadastrais`
--

INSERT INTO `dadoscadastrais` (`id`, `id_usuario`, `cpf`, `dataNascimento`, `telefone`, `telefone2`, `cep`, `endereco`, `numero`, `complemento`, `bairro`, `estado`, `cidade`, `NomeCompleto`) VALUES
(1, 9, '005.627.120-46', '19/10/1996', '(00) 00000-0000', '(00) 00000-0000', '00000-000', 'rua hugo porto', '106', 'teste', 'tes', 'RS', 'Pelotas', 'Patrick de Medeiros Fernandes'),
(2, 1, '000.000.000-00', '00/00/0000', '(00) 00000-0000', '(00) 00000-0000', '00000-000', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 12, '120.465.210-12', '10/10/1999', '(53) 99999-9999', '(53) 99999-9999', '96065-845', 'Rua Hugo Porto', '10', '', 'Três Vendas', 'RS', 'Pelotas', 'Lucas Rolin'),
(8, 20, '444.444.444-44', '44/44/4444', '(44) 44444-4444', '', '69908-056', 'Rua Almeida Barbosa', '42', '', 'Belo Jardim II', 'AC', 'Rio Branco', 'Lucas Rolin'),
(9, 21, '544.564.687-67', '15/12/1998', '(23) 42342-3423', '(22) 42423-4233', '45993-350', 'Rua Almeida Garret', '42', '', 'Arco Verde', 'BA', 'Teixeira de Freitas', 'Lucas Rolin'),
(10, 22, '000.000.000-00', '00/00/0000', '(00) 00000-0000', '(00) 00000-0000', '00000-000', NULL, NULL, NULL, NULL, NULL, NULL, 'Jovem Otaku');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` (`id`, `nome`) VALUES
(1, 'filme'),
(2, 'trilha sonora'),
(3, 'direção'),
(4, 'figurino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `nivel` int(1) NOT NULL,
  `id_google` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `nivel`, `id_google`) VALUES
(1, 'Jovem ', 'patrickmedferpf@bol.com.br', 'd9b1d7db4cd6e70935368a1efb10e377', 3, ''),
(9, 'Patrick Fernandes', 'patrickmedferpf@gmail.com', NULL, 1, '101066498643971114910'),
(12, 'lúcáçç ..e', 'lucasrolim21@gmail.com', 'd9b1d7db4cd6e70935368a1efb10e377', 1, '116502191122185536040'),
(20, 'teste', 'patrickmedferpf@bol.com', 'd9b1d7db4cd6e70935368a1efb10e377', 1, NULL),
(21, 'teste45', 'patrickmedferpf@bol.com', 'ec6a6536ca304edf844d1d248a4f08dc', 1, NULL),
(22, 'Jovem Otaku', 'umjovemotaku@gmail.com', NULL, 1, '118024098029037421026');

-- --------------------------------------------------------

--
-- Estrutura da tabela `voto`
--

CREATE TABLE `voto` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `mensagem` text NOT NULL,
  `NomeFilme` varchar(100) NOT NULL,
  `LinkImgFilme` varchar(250) NOT NULL,
  `GeneroFilme` varchar(100) NOT NULL,
  `idFilme` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `voto`
--

INSERT INTO `voto` (`id`, `user_id`, `tipo_id`, `mensagem`, `NomeFilme`, `LinkImgFilme`, `GeneroFilme`, `idFilme`) VALUES
(2, 9, 2, 'NULL', 'ANIMAIS FANTÁSTICOS: OS CRIMES DE GRINDELWALD', 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1532959780.jpg', 'Aventura', 16),
(3, 9, 3, 'NULL', 'VINGADORES: GUERRA INFINITA', 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521581139.jpg', 'Ação', 9),
(4, 9, 1, 'NULL', 'VINGADORES: GUERRA INFINITA', 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521581139.jpg', 'Ação', 9),
(6, 22, 4, 'NULL', 'VINGADORES: GUERRA INFINITA', 'https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/69/1080x1620_1521581139.jpg', 'Ação', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dadoscadastrais`
--
ALTER TABLE `dadoscadastrais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_usuario_2` (`id_usuario`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `id_google` (`id_google`);

--
-- Indexes for table `voto`
--
ALTER TABLE `voto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `tipo_id` (`tipo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dadoscadastrais`
--
ALTER TABLE `dadoscadastrais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `voto`
--
ALTER TABLE `voto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `dadoscadastrais`
--
ALTER TABLE `dadoscadastrais`
  ADD CONSTRAINT `dadoscadastrais_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `voto`
--
ALTER TABLE `voto`
  ADD CONSTRAINT `voto_ibfk_1` FOREIGN KEY (`tipo_id`) REFERENCES `tipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `voto_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

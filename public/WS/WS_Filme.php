<?php

header('Access-Control-Allow-Origin: *');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "cinema";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$extra = "where 1 ";
if (isset($_GET['ano'])) {
    $extra .= " and ano = '" . $_GET['ano'] . "'";
} else {
    $extra .= " ";
}

if (isset($_GET['genero'])) {
    $extra .= " and genero = '" . $_GET['genero'] . "'";
} else {
    $extra .= " ";
}

if (isset($_GET['id'])) {
    $extra .= " and id = '" . $_GET['id'] . "'";
} else {
    $extra .= " ";
}


if (isset($_GET['nome'])) {
    $extra .= " and `nome` LIKE '%" . $_GET['nome'] . "'%";
} else {
    $extra .= " ";
}

if (isset($_GET['classificacao'])) {
    $extra .= " and `nome` LIKE '%" . $_GET['classificacao'] . "'%";
} else {
    $extra .= " ";
}

 $sql = "SELECT * FROM filme  $extra";
$result = $conn->query($sql);
$horarios = array();
if ($result->num_rows > 0) {
// output data of each row
    while ($row = $result->fetch_assoc()) {
        $horarios[] = array("id" => utf8_encode($row['id']),
        "nome" => utf8_encode($row["nome"]),
        "descricao" => utf8_encode($row["descricao"]),
        "classificacao" => utf8_encode($row["classificacao"]),
        "genero" => utf8_encode($row["genero"]),
        "duracao" => utf8_encode($row["duracao"]),
        "linkImg" => utf8_encode($row["linkImg"]),
        "ano" => utf8_encode($row["ano"])

        );
    }
}
$conn->close();
//$carros = array("carros"=> $carros);
echo json_encode($horarios, JSON_PRETTY_PRINT);

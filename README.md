# Trabalho de Sistemas Distribuidos



## **Sistema de concurso para um cinema**
 

## **Resumo**
Este projeto é um sistema de um concurso de um cinema, 
que pega dados de filmes, por uma API, disponibillizado pelo sistema deste cinema feito neste projeto, que busca os dados 
e cadastra em outro banco de dados os dados do filme e do voto do participante.




### **Documentação**
O contesto e o que este projeto, e por em si sua documentação se encontra na **[Wiki](https://gitlab.com/Patrick-Fernandes/trabalho-de-sistemas-distribuidos/wikis/home)** deste projeto.








### **Licença**
Adicionada a licença GNU General Public Licence v3.0




### *Linguagens e Tecnologias que serão utilizadas:*
A seguir serão apresentados as linguagens e tecnologias que serão utilizadas.
<ul>
<li><a href="https://secure.php.net/manual/pt_BR/migration70.php"><B>PHP 7.0</B></a></li>
<li><a href="http://getbootstrap.com/getting-started/"><B>Bootstrap 3.3.7</B></a></li>
<li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.7/en/"><B>MySQL 5.7</B></a></li>
<li><a href="https://dev.w3.org/html5/html-author/"><B>HTML 5</B></a></li>
<li><a href="http://devdocs.io/css/"><B>CSS 3</B></a></li>
<li><a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/New_in_JavaScript/1.8.5"><B>JavaScript 1.8.5</B></a></li>
<li><a href="https://viacep.com.br/"><B>Via Cep</B></a></li>
<li><a href="https://developers.google.com/identity/sign-in/web/sign-in"><B>Google Login</B></a></li>



</ul>



### *Autor*
Patrick de Medeiros Fernandes


